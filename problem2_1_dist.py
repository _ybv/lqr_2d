import numpy as np
import sys


import matplotlib.pyplot as plt
"""Intro to robot algorithms - assignment 2- problem 2"""
"""Plots values between x and time """
""" Starts at x=1 and x_vel=0 """

def main():
	dT= 0.025
	#T=1000
	b=1
	u_1=0
	u_2=0
	arr = {}
	arr1 = {}
	arr2={}
	xt = np.matrix([[1],[0]])
	Q=np.eye(2, dtype=int)
	Q_F= 100*Q
	#print Q_F
	R=np.eye(1, dtype=int)
	#print R
	#A=np.matrix('1 0 dT 0 ; 0 1 0 dT ; 0 1*b 0 dT*b ; 0 0 0 0')
	A= np.matrix([[1,dT], [b*dT,1]])
	At= np.transpose(A)
	#print At
	#print A
	B = np.matrix([[0],[dT]])
	Bt=np.transpose(B)
	#print B
	p_t = Q_F
	p=p_t
	#print p_t
	#(((At*p_t*B)*(np.linalg.inv(R+Bt*p_t*B)))*(Bt*p_t*A))
	for T in range(999, -1, -1):
		arr[T]=p
		#p = Q+(At*p_t*A)-(((At*p_t*B)*(np.linalg.inv(R+Bt*p_t*B)))*(Bt*p_t*A))
		p=Q + np.dot(np.dot(At,p_t),A) - np.dot(np.dot(np.dot(np.dot(At,p_t),B), np.linalg.inv(R + np.dot(np.dot(Bt,p_t),B))), np.dot(np.dot(Bt,p_t),A))
		p_t=p
	#print arr
	f = open('21.txt', 'r+b')
	f.write('values between x and x_vel\n')
	for l in range(0,1000):
		s = str(xt[0])+","+str(xt[1])+"at time:"+str(l)+"\n"
		f.write(s)
		k = (-1)*np.dot(np.linalg.inv(R + np.dot(np.dot(Bt,p_t),B)),np.dot(np.dot(Bt,arr[l]),A))
		ut = np.dot(k,xt)
		arr2[l]=ut
		xtp=np.dot(A,xt)+np.dot(B,arr2[l])
		xt=xtp
		pylab.ylabel('x displacement')
  		pylab.xlabel('time steps')
  		pylab.title('Plots values between x and time')
		plt.plot(l,xt[0],'o')


        
if __name__ == '__main__':
    main()